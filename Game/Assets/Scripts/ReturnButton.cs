﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ReturnButton : MonoBehaviour {

	// Use this for initialization
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(() => returnmenu());
    }
    void returnmenu()
    {
        SceneManager.LoadScene("Menu");
        ballcontroller.score1 = 0;
        ballcontroller.score2 = 0;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
