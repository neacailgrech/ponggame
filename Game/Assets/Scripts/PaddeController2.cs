﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddeController2 : MonoBehaviour {
    // Use this for initialization
    public float verticalSpeed = 0.5F;
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 mousePosition = Input.mousePosition;
        Vector3 paddlePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        transform.position = new Vector3(8,Mathf.Clamp(paddlePosition.y, -4, 4), 0);
    }
}
