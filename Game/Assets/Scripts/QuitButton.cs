﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class QuitButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Button>().onClick.AddListener(() => quitgame());
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    void quitgame()
    {
        EditorApplication.Exit(0);
    }
}
