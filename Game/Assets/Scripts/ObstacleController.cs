﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour {
    bool high = true;
    Vector2 startingPosition;
	// Use this for initialization
	void Start () {
        startingPosition = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (startingPosition.y >= 5)
        {
            high = false;
        }else if (startingPosition.y <= -5)
        {
            high = true;
        }
        if (high == true)
        {
            startingPosition.y += 0.05f;

        }
        else
        {
            startingPosition.y += -0.05f;
        }
        transform.position = startingPosition;
    }
}
