﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaddeController1 : MonoBehaviour
{
    // Use this for initialization
    GameObject ball;
    public float paddlespeed = 0.4f;
    public Vector3 playerpos;

    public bool gameStarted = false;

    void Start()
    {
        ball = GameObject.Find("ball");
    }

    // Update is called once per frame
    void Update()
    {
        float movement = (Input.GetAxis("Vertical") * paddlespeed);
        float yPos = gameObject.transform.position.y + movement;
        playerpos = new Vector3(-8, Mathf.Clamp(yPos, -4, 4), 0);
        gameObject.transform.position = playerpos;

        if (gameStarted == false)
        {
            
            ball.transform.position = transform.position + new Vector3(0.5f, 0f);
        }
        
        if (Input.GetMouseButtonDown(0) && gameStarted == false)
        {
            gameStarted = true;
            float randommovement = Random.Range(-9f, 9f);
            ball.GetComponent<Rigidbody2D>().velocity = new Vector2(13f,randommovement);
        }
    }

}