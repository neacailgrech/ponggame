﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ballcontroller : MonoBehaviour {
    public static int score1 = 0;
    public static int score2 = 0;

    public Text playerscore1;
    public Text playerscore2;

    GameObject leftpost;
    GameObject rightpost;


    GameObject ball;
    // Use this for initialization
    void Start() {
        leftpost = GameObject.FindGameObjectWithTag("leftpost");
        rightpost = GameObject.FindGameObjectWithTag("rightpost");
        ball = GameObject.FindGameObjectWithTag("ball");

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

        float randommovement = Random.Range(-0.5f, 1f);
        GetComponent<Rigidbody2D>().velocity += new Vector2(-0.5f, randommovement);
    }



    // Update is called once per frame
    void Update() {
        playerscore1.text = "" + score1;
        playerscore2.text = "" + score2;

        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;
        //Level 1
        if (sceneName == "Game")
        {
            if (score1 == 3 || score2 == 3)
            {
                SceneManager.LoadScene("Game2");
            }
        }
        //Level 2;
        if (sceneName == "Game2")
        {
            if (score1 == 6 || score2 == 6)
            {
                SceneManager.LoadScene("Game3");
               
            }
        }
        if(sceneName == "Game3")
        {
            if(score1==8 || score2 == 8)
            {
                SceneManager.LoadScene("Win");
            }
        }

}



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "leftpost")
        {
            score2 += 1;
            StartCoroutine(WaitGoal1());

        }
        if (collision.tag == "rightpost")
        {
            score1 += 1;
            StartCoroutine(WaitGoal2());

        }
    }
    IEnumerator WaitGoal1()
    {
        ball.transform.position = new Vector2(0f, 0f);
        ball.GetComponent<Rigidbody2D>().velocity = new Vector2(0f,0f);
        yield return new WaitForSeconds(3f);
        float randommovement = Random.Range(-9f, 9f);
        ball.GetComponent<Rigidbody2D>().velocity = new Vector2(-13f, randommovement);

    }
    IEnumerator WaitGoal2()
    {
        ball.transform.position = new Vector2(0f, 0f);
        ball.GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
        yield return new WaitForSeconds(3f);
        float randommovement = Random.Range(-9f, 9f);
        ball.GetComponent<Rigidbody2D>().velocity = new Vector2(13f, randommovement);

    }
}
